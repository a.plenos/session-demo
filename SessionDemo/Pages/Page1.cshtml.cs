using System;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Http;

namespace SessionDemo.Pages
{
    public class Page1Model : PageModel
    {
        string name;
        int age;
        public void OnGet()
        {
        }

        public void OnPost()
        {
            name = Request.Form["Name"];
            age = Convert.ToInt32(Request.Form["Age"]);

            HttpContext.Session.SetString("Name", name);
            HttpContext.Session.SetInt32("Age", age);
        }
    }
}
