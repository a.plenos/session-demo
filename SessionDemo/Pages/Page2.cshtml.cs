using System;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Http;

namespace SessionDemo.Pages
{

    public class Page2Model : PageModel
    {
        public string name;
        public int age;

        public void OnGet()
        {
            name = (HttpContext.Session.GetString("Name") != null) ? HttpContext.Session.GetString("Name") : null;
            age = (HttpContext.Session.GetInt32("Age") != null) ? Convert.ToInt32(HttpContext.Session.GetInt32("Age")) : -1;

        }
    }
}
